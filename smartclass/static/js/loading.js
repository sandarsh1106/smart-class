﻿function loadingAnimation() {
    let form = $('#id_valid_form');
    form.validate();
    if(form.valid()){
        let loading = '<div class="loading"><div class="animation-holder"><img src="/static/img/loading-icon.png" alt="loading-icon"/></div></div>';
        $('body').append(loading);
    }
}

function confirmDialog(message) {
    if (confirm(message)) {
        return loadingAnimation();
    }
    return false;
}