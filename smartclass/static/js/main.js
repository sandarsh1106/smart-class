$(function(){
  let search_id = $('#search');
  search_id.keyup(function() {
      if (!search_id.val()) {
            $('#search-results').css('display', 'none');
            return;
        }
        else {
          $('#search-results').css('display', 'block');
      }
      $.ajax({
          type: "POST",
          url: "search-everything/",
          data: {
              'search_text': search_id.val(),
              'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
          },
          success: searchSuccess,
          dataType: 'html'
      })
  })
});

function searchSuccess(data, textStatus, jqXHR) {
    $('#search-results').html(data);
}

$('#search').change(function () {
   $('.searchResults').show();
});

let validImgExt = ['jpg', 'jpeg', 'png'];
let validVidExt = ['mp4', 'avi', 'quicktime', 'x-ms-wmv', 'x-flv'];

$('#image-uploads').on('change', function(){
  let extension = this.files[0].type.split('/')[1];
  console.log(this.files[0].type);
  if(validImgExt.indexOf(extension) === -1){
    alert('Not a valid image. You may only upload .jpg, .jpeg and .png files.');
    $('#image-uploads').val("");
  }
});

$('#video-upload').on('change', function(){
  let extension = this.files[0].type.split('/')[1];
  console.log(this.files[0].type);
  if(validVidExt.indexOf(extension) === -1){
    alert('Not a valid video file. You may only upload .mp4, .avi, .mov, .flv and .wmv files.');
    $('#video-upload').val("");
  }
});

function ajax_validate(id, url, form) {
            return $.ajax({
                cache: false,
                url: form.attr(url),
                data: form.serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.is_exists){
                        alert(data.error_message);
                        $(id).val("");
                    }
                }
            });
        }

function ajax_like(url) {
    $.ajax({
        type: "POST",
        url: url,
        data: {
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
        },
        success: function () {

        },
        dataType: 'html'
    })
}

function update(btn, data){
    var id = $('#likeIcon');
    $('#like-count').html(data.like_count);
    if(!data.liked){
        id.addClass('gray-thumbs-up');
        id.removeClass('black-thumbs-up');
    }
    else{
        id.addClass('black-thumbs-up');
        id.removeClass('gray-thumbs-up');
    }
}
$('.like-button').click(function(e){
   e.preventDefault();
   var this_ = $(this);
   var likeUrl = this_.attr("data-href");
   $.ajax({
       url: likeUrl,
       method: "GET",
       data:{},
       success: function (data) {
           var newLikes;
           update(this_, data);
       },
       error: function (error) {
           console.log(error);
       }
   });
});