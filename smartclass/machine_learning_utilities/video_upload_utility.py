import subprocess
import datetime
from PIL import Image
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FFPROBE = os.path.join(BASE_DIR, 'machine_learning_utilities/ffprobe.exe')
FFMPEG = os.path.join(BASE_DIR, 'machine_learning_utilities/ffmpeg.exe')


# Function to save a file.
def file_save(file_path, file):
    with open(file_path, 'wb+') as destination:
        for chunk in file.chunks():
            # write to destination.
            destination.write(chunk)


# Function to get the duration of the video.
def get_video_duration(input_video):
    # Call a subprocess command to get the duration with the help of FFPROBE
    cmd = '{0} -i {1} -show_entries format=duration -v quiet -of csv="p=0"'.format(FFPROBE, input_video)
    output = subprocess.check_output(
        cmd,
        shell=True,  # Let this run in the shell
        stderr=subprocess.STDOUT
    )
    m, s = divmod(int(float(output)), 60)
    h, m = divmod(m, 60)
    if h > 0:
        return "{0}h {1}m".format(h, m)
    else:
        return "{0}m {1}s".format(m, s)


# Function to create a thumbnail off of an image.
def make_thumbnail(file_path, filename):
    target_size = 320
    orig_img = Image.open(file_path + os.path.sep + filename)
    img = orig_img

    # calculate target height of the resized image to maintain the aspect ratio
    wpercent = (target_size / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))

    # perform resizing
    img = img.resize((target_size, hsize), Image.LANCZOS)

    # save the resized image to the output dir with a modified file name
    new_filename = "thumbnail.png"
    img.save(file_path + os.path.sep + new_filename)


# Function to extract a frame from the video and save it.
def extract_image_from_video(video, destination_image_name):
    subprocess.call([FFMPEG, '-i', video, '-ss', '00:00:03', '-vframes',
                     '1', '-f', 'image2', destination_image_name])
