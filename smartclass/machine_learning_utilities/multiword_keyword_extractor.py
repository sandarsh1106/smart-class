from collections import Counter
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, MWETokenizer
from nltk.stem import WordNetLemmatizer
import os
import subprocess

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
audio_to_text = os.path.join(BASE_DIR, 'machine_learning_utilities/audio_to_text.py')


# Removes roman digits from the text.
def is_roman(word):
    valid_roman_numerals = ["M", "D", "C", "L", "X", "V", "I", "(", ")"]
    for ch in word:
        if ch.upper() not in valid_roman_numerals:
            return False
    return True


# Extracts glossary terms from image to list using OCR.
def glossary_extract_to_string(tesseract_text):
    extracted_phrases = set()
    extracted_phrases_string = ""

    ''' Split the lines into phrases on a comma. Remove numbers, unnecessary spaces, and
    when a phrase extends to a new line, place that phrase into a temp variable and then concatenate it with the
    next immediate phrase. Finally, remove phrases that are single letters and roman numbers.'''
    temp = ""
    for line in tesseract_text.split('\n'):
        if line is "":
            continue
        split_at_comma = line.split(',')
        for phrase in split_at_comma:

            # This is to remove '-' from numbers when the indexes are given in page ranges. Example: 123-125.
            x = phrase.replace('-', '').strip()
            if not x.isdigit():
                if not phrase.endswith('-'):
                    phrase = temp + phrase
                    temp = ""
                    extracted_phrases.add(phrase.strip())
                else:
                    temp = phrase.replace('-', '')
    for phrase in list(extracted_phrases):
        if not len(phrase.split()) > 1 and is_roman(phrase):
            extracted_phrases.remove(phrase)
        else:
            extracted_phrases_string += phrase + ", "
    return extracted_phrases_string


def keywords_extractor_from_video(phrases_from_index_files, video_file):

    important_phrases = list(phrases_from_index_files)
    file_name = os.path.abspath(os.path.join(video_file, '..')) + r'\transcript.txt'
    filtered_glossary = []

    # Call the video to text generation routine.
    subprocess.call(['python', audio_to_text, '-S', 'en', '-D', 'en', video_file])
    lemmatizer = WordNetLemmatizer()
    with open(file_name) as file:
        text = file.read()

        # Remove the apostrophe 's' from the text and convert it to lower case for processing purposes.
        lower_cased_text = text.replace("'s", "").lower()

        # This special stop words list contains over 500 stop words. This can be changed according to the app's needs.
        stop_words = set(stopwords.words('smartstoplist'))

        ''' MWETokenizer will generate multi word tokens based on the input tuples. Here, we add all the key phrases
        we extracted from the glossary in tuples to generate the MWE pre-index. Using these pre-indices, the tokenizer
        will be able to recognize those exact word sets and generate the tokens intelligently.'''
        tokenizer = MWETokenizer()
        for phrase in important_phrases:
            tokens = word_tokenize(phrase)
            tokenizer.add_mwe(tuple(tokens))
        word_tokens = tokenizer.tokenize(lower_cased_text.split())

        # Remove stop words from the subtitle.
        filtered_sentence_list = []
        [filtered_sentence_list.append(w) for w in word_tokens if w not in stop_words and not w.isdigit()]

        ''' MWE tokenizer generates the multi word tokens by placing an '_' between the words. Here, we change them back
        to spaces for better user experience.'''
        filtered_sentence_list[:] = [s.replace('_', ' ') for s in filtered_sentence_list]

        # Extract the keywords from the filtered subtitle and lemmatize them to get the root words out of those
        # keywords.
        keywords = [lemmatizer.lemmatize(x) for x, _ in Counter(filtered_sentence_list).most_common(15)]

        # Add the phrases used from glossary regardless of their usage frequency.
        [keywords.append(ip) for ip in filtered_glossary if ip in filtered_sentence_list]

    return ', '.join(set(keywords))
