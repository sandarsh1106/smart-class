import sys
import argparse
import multiprocessing
import os
import math
import wave
import audioop
import pysrt
import requests
import json
import tempfile
import subprocess
from googleapiclient.discovery import build
from webvtt import WebVTT
import re

GOOGLE_SPEECH_API_KEY = "AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw"
GOOGLE_SPEECH_API_URL = "http://www.google.com/speech-api/v2/recognize?client=chromium&lang={lang}&key={key}"
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DESTINATION = os.path.join(BASE_DIR, 'media_assets/temp_dir')
FFMPEG = os.path.join(BASE_DIR, 'machine_learning_utilities/ffmpeg.exe')

text_type = str if sys.version_info < (3,) else bytes


class FLACConverter:
    def __init__(self, source_path, include_before=0.25, include_after=0.25):
        self.source_path = source_path
        self.include_before = include_before
        self.include_after = include_after

    def __call__(self, region):
        try:
            start, end = region
            start = max(0, start - self.include_before)
            end += self.include_after
            temp = tempfile.NamedTemporaryFile(suffix='.flac', delete=False)
            command = [FFMPEG, "-y", "-i", self.source_path,
                       "-ss", str(start), "-t", str(end - start),
                       "-loglevel", "error", temp.name]
            subprocess.check_output(command)

            return temp.read()

        except KeyboardInterrupt:
            return


class SpeechRecognizer:
    def __init__(self, language="en", rate=44100, retries=3, api_key=GOOGLE_SPEECH_API_KEY):
        self.language = language
        self.rate = rate
        self.api_key = api_key
        self.retries = retries

    def __call__(self, data):
        try:
            for i in range(self.retries):
                url = GOOGLE_SPEECH_API_URL.format(lang=self.language, key=self.api_key)
                headers = {'Content-Type': 'audio/x-flac; rate=%d' % self.rate}

                try:
                    resp = requests.post(url, data=data, headers=headers)
                except requests.exceptions.ConnectionError:
                    continue

                for line in resp.content.decode('utf-8').split('\n'):
                    try:
                        line = json.loads(line)
                        line = line['result'][0]['alternative'][0]['transcript']
                        return line[:1].upper() + line[1:]
                    except:
                        continue

        except KeyboardInterrupt:
            return


class Translator:
    def __init__(self, language, api_key, src, dst):
        self.language = language
        self.api_key = api_key
        self.service = build('translate', 'v2', developerKey=self.api_key)
        self.src = src
        self.dst = dst

    def __call__(self, sentence):
        try:
            if not sentence: return
            result = self.service.translations().list(
                source=self.src,
                target=self.dst,
                q=[sentence]
            ).execute()
            if 'translations' in result and len(result['translations']) and \
                    'translatedText' in result['translations'][0]:
                return result['translations'][0]['translatedText']
            return ""

        except KeyboardInterrupt:
            return


def force_unicode(s, encoding="utf-8"):
    if isinstance(s, text_type):
        return s.decode(encoding)
    return s


def percentile(arr, percent):
    arr = sorted(arr)
    k = (len(arr) - 1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c: return arr[int(k)]
    d0 = arr[int(f)] * (c - k)
    d1 = arr[int(c)] * (k - f)
    return d0 + d1


def is_same_language(lang1, lang2):
    return lang1.split("-")[0] == lang2.split("-")[0]


def extract_audio(filename, channels=1, rate=16000):
    temp = tempfile.NamedTemporaryFile(suffix='.wav', delete=False)
    command = [FFMPEG, "-y", "-i", filename, "-ac", str(channels), "-ar", str(rate), "-loglevel", "error", temp.name]
    subprocess.check_output(command)
    return temp.name, rate


def find_speech_regions(filename, frame_width=4096, min_region_size=0.5, max_region_size=6):
    reader = wave.open(filename)
    sample_width = reader.getsampwidth()
    rate = reader.getframerate()
    n_channels = reader.getnchannels()

    total_duration = reader.getnframes() / rate
    chunk_duration = float(frame_width) / rate

    n_chunks = int(total_duration / chunk_duration)
    energies = []

    for i in range(n_chunks):
        chunk = reader.readframes(frame_width)
        energies.append(audioop.rms(chunk, sample_width * n_channels))

    threshold = percentile(energies, 0.2)

    elapsed_time = 0

    regions = []
    region_start = None

    for energy in energies:
        elapsed_time += chunk_duration

        is_silence = energy <= threshold
        max_exceeded = region_start and elapsed_time - region_start >= max_region_size

        if (max_exceeded or is_silence) and region_start:
            if elapsed_time - region_start >= min_region_size:
                regions.append((region_start, elapsed_time))
            region_start = None

        elif (not region_start) and (not is_silence):
            region_start = elapsed_time

    return regions


def srt_formatter(subtitles, show_before=0, show_after=0):
    f = pysrt.SubRipFile()
    for i, (rng, text) in enumerate(subtitles, 1):
        item = pysrt.SubRipItem()
        item.index = i
        item.text = force_unicode(text)
        start, end = rng
        item.start.seconds = max(0, start - show_before)
        item.end.seconds = end + show_after
        f.append(item)
    return '\n'.join(map(str, f))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('source_path', help="Path to the video or audio file to subtitle", nargs='?')
    parser.add_argument('-C', '--concurrency', help="Number of concurrent API requests to make", type=int, default=10)
    parser.add_argument('-o', '--output', help="Output path for subtitles (by default, subtitles are saved in \
                        the same directory and name as the source path)")
    parser.add_argument('-F', '--format', help="Destination subtitle format", default="srt")
    parser.add_argument('-S', '--src-language', help="Language spoken in source file", default="en")
    parser.add_argument('-D', '--dst-language', help="Desired language for the subtitles", default="en")
    parser.add_argument('-K', '--api-key',
                        help="The Google Translate API key to be used. (Required for subtitle translation)")
    parser.add_argument('--list-formats', help="List all available subtitle formats", action='store_true')
    parser.add_argument('--list-languages', help="List all available source/destination languages", action='store_true')

    args = parser.parse_args()

    audio_filename, audio_rate = extract_audio(args.source_path)
    regions = find_speech_regions(audio_filename)

    pool = multiprocessing.Pool(args.concurrency)
    converter = FLACConverter(source_path=audio_filename)
    recognizer = SpeechRecognizer(language=args.src_language, rate=audio_rate, api_key=GOOGLE_SPEECH_API_KEY)
    transcripts = []
    if regions:
        try:
            extracted_regions = []
            for i, extracted_region in enumerate(pool.imap(converter, regions)):
                extracted_regions.append(extracted_region)

            for i, transcript in enumerate(pool.imap(recognizer, extracted_regions)):
                transcripts.append(transcript)

            if not is_same_language(args.src_language, args.src_language):
                if args.api_key:
                    google_translate_api_key = args.api_key
                    translator = Translator(args.dst_language, google_translate_api_key, dst=args.dst_language,
                                            src=args.src_language)
                    translated_transcripts = []
                    for i, transcript in enumerate(pool.imap(translator, transcripts)):
                        translated_transcripts.append(transcript)
                    transcripts = translated_transcripts
                else:
                    return 1

        except KeyboardInterrupt:
            return 1

    timed_subtitles = [(r, t) for r, t in zip(regions, transcripts) if t]
    formatted_subtitles = srt_formatter(timed_subtitles)

    destination = args.output
    dest_text_file = args.output
    if not destination:
        destination = os.path.join(DESTINATION, "transcript.{format}".format(format=args.format))
        dest_text_file = os.path.join(DESTINATION, "transcript.{format}".format(format="txt"))

    with open(destination, 'wb') as f:
        f.write(formatted_subtitles.encode('utf-8'))

    webvtt = WebVTT().from_srt(destination)
    webvtt.save()

    with open(os.path.join(DESTINATION, "transcript.{format}".format(format="vtt")), "r") as vtt:
        content = vtt.readlines()

    new_content = []
    count = 1
    for line in content:
        if not line.strip():
            new_content.append(line.replace("\n", "\n" + str(count) + "\n"))
            count += 1
        else:
            new_content.append(line)

    f = open(os.path.join(DESTINATION, "transcript.{format}".format(format="vtt")), 'r+')
    f.truncate()

    with open(os.path.join(DESTINATION, "transcript.{format}".format(format="vtt")), "w") as vtt:
        for line in new_content:
            vtt.write(line)

    srt_file = open(destination, "r").read()
    with open(dest_text_file, "w+") as dtf:
        result = re.findall("(\d+:\d+:\d+,\d+ --> \d+:\d+:\d+,\d+)\s+(.+)", srt_file)
        [dtf.write(x + '\n') for i, x in result]
    # print("Subtitles file created at {}".format(destination))
    os.remove(audio_filename)
    return 0


if __name__ == '__main__':
    sys.exit(main())

