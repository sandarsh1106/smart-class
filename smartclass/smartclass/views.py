from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.models import CharField, Value
from django.db.models.functions import Concat

from faq_video_portal import models as fp


def welcome(request):
    if request.user.is_authenticated:
        return redirect('user_home')
    else:
        return render(request, 'smartclass/welcome.html')


# Function to search everything.
@login_required
def search_anything(request):
    if request.method == "POST":
        search_text = request.POST['search_text']
    else:
        search_text = ''
    topics = fp.Topic.objects.video_search(search_text)
    courses = fp.Course.objects.course_search(search_text)
    faqs = fp.FrequentlyAskedQuestion.objects.faq_search(search_text)
    author = User.objects.annotate(full_name=Concat('first_name', Value(' '), 'last_name'))
    authors = author.filter((Q(username__startswith=search_text) |
                             Q(first_name__startswith=search_text) |
                             Q(last_name__startswith=search_text) |
                             Q(full_name__startswith=search_text)) &
                            Q(is_staff=True))
    return render(request, 'smartclass/search/live_search.html', {'topics': topics,
                                                                  'courses': courses,
                                                                  'faqs': faqs,
                                                                  'authors': authors})


@login_required
def full_search_results_display(request):
    if request.method == "POST":
        search_text = request.POST['search_text']
        topics = fp.Topic.objects.video_search(search_text)
        courses = fp.Course.objects.course_search(search_text)
        faqs = fp.FrequentlyAskedQuestion.objects.faq_search(search_text)
        author = User.objects.annotate(full_name=Concat('first_name', Value(' '), 'last_name'))
        authors = author.filter((Q(username__startswith=search_text) |
                                 Q(first_name__startswith=search_text) |
                                 Q(last_name__startswith=search_text) |
                                 Q(full_name__startswith=search_text) |
                                 Q(topic__title__contains=search_text) |
                                 Q(topic__hash_tags__contains=search_text) |
                                 Q(topic__video_keywords__contains=search_text) |
                                 Q(course__course_no__contains=search_text) |
                                 Q(course__course_name__contains=search_text) |
                                 Q(course__hash_tags__contains=search_text) |
                                 Q(frequentlyaskedquestion__title__contains=search_text) |
                                 Q(frequentlyaskedquestion__hash_tags__contains=search_text) |
                                 Q(frequentlyaskedquestion__video_keywords__contains=search_text)) &
                                Q(is_staff=True)).distinct()
        return render(request, 'smartclass/search/search_results.html', {'topics': topics,
                                                                         'courses': courses,
                                                                         'faqs': faqs,
                                                                         'authors': authors})
    else:
        return redirect('user_home')
