from django.contrib import admin
from django.urls import re_path, include, path
from django.conf import settings
from django.conf.urls.static import static

from .views import welcome, search_anything, full_search_results_display

urlpatterns = [
    re_path(r'^$', welcome, name="smartclass_welcome"),
    re_path(r'search-everything/$', search_anything, name="search_everything"),
    re_path(r'search-results/$', full_search_results_display, name="search_results"),
    path('admin/', admin.site.urls),
    path('', include('faq_video_portal.urls')),
    path('', include('users.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'Smart Class Admin Panel'
admin.site.site_title = 'Smart Class Panel'
admin.site.index_title = 'Welcome to Smart Class Admin Panel'
