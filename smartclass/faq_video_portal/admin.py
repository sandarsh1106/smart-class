from django.contrib import admin

from .models import Course, Topic, FrequentlyAskedQuestion


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('id', 'course_no', 'course_name', 'created_by', 'created_on')
    search_fields = ('course_no', 'course_name', 'created_by__username')


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'added_by', 'difficulty_level', 'added_on', 'modified_on')
    search_fields = ('title', 'added_by__username', 'difficulty_level')


@admin.register(FrequentlyAskedQuestion)
class FAQAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'added_by', 'added_on', 'modified_on')
    search_fields = ('title', 'added_by__username', 'difficulty_level')
