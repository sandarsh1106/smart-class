from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.db.models import Q
from django.db.models import Value
from django.db.models.functions import Concat

from operator import or_
from functools import reduce

DIFFICULTY_LEVELS = (
    ('1', 'Beginner'),
    ('2', 'Intermediate'),
    ('3', 'Advance'),
    ('4', 'Expert'),
)


class VideoQuerySet(models.QuerySet):
    def video_search(self, search_text):
        topic = self.annotate(added_by_full_name=Concat('added_by__first_name', Value(' '), 'added_by__last_name'))
        return topic.filter(
            Q(title__icontains=search_text) |
            Q(hash_tags__icontains=search_text) |
            Q(video_keywords__icontains=search_text) |
            ((Q(added_by__first_name__startswith=search_text) |
              Q(added_by__last_name__startswith=search_text) |
              Q(added_by_full_name__startswith=search_text)) &
             Q(added_by__is_staff=True))
        ).order_by('difficulty_level')

    def faq_for_this_course(self, topics):
        return self.filter(
            Q(topic__in=topics)
        )

    def faq_search(self, search_text):
        faq = self.annotate(added_by_full_name=Concat('added_by__first_name', Value(' '), 'added_by__last_name'))
        return faq.filter(
            Q(title__icontains=search_text) |
            Q(hash_tags__icontains=search_text) |
            Q(video_keywords__icontains=search_text) |
            ((Q(added_by__first_name__startswith=search_text) |
              Q(added_by__last_name__startswith=search_text) |
              Q(added_by_full_name__startswith=search_text)) &
             Q(added_by__is_staff=True))
        )

    def related_videos(self, hash_tags):
        current_video_tags = hash_tags.strip().split(',')
        condition = reduce(or_, [Q(hash_tags__contains=tags) for tags in current_video_tags])
        return self.filter(condition)


class CourseQuerySet(models.QuerySet):
    def course_search(self, search_text):
        course = self.annotate(created_by_full_name=Concat('created_by__first_name', Value(' '),
                                                           'created_by__last_name'))
        return course.filter(
            Q(course_no__contains=search_text) |
            Q(course_name__contains=search_text) |
            Q(hash_tags__contains=search_text) |
            ((Q(created_by__first_name__startswith=search_text) |
              Q(created_by__last_name__startswith=search_text) |
              Q(created_by_full_name__startswith=search_text)) &
             Q(created_by__is_staff=True))
        ).order_by('-created_on')

    def related_courses(self, hash_tags):
        current_course_tags = hash_tags.strip().split(',')
        condition = reduce(or_, [Q(hash_tags__contains=tags) for tags in current_course_tags])
        return self.filter(condition)


class Course(models.Model):
    course_no = models.CharField(max_length=5,
                                 verbose_name="Course number",
                                 help_text="Please add a course number for this course.",
                                 blank=False,
                                 null=False)
    course_name = models.CharField(max_length=75,
                                   verbose_name="Course name",
                                   help_text="Please add a course name for this course.",
                                   blank=False,
                                   null=False)
    created_by = models.ForeignKey(User,
                                   verbose_name="Name of the instructor",
                                   on_delete=models.CASCADE)
    course_description = models.TextField(max_length=1000, verbose_name="Course description",
                                          help_text="Please add a description for this course.")
    hash_tags = models.CharField(max_length=150,
                                 verbose_name="Tags",
                                 help_text="Please add some tags separated by commas for"
                                           " better search capabilities. "
                                           " e.g. Programming, Design, Data Science, Neural Networks ",
                                 blank=False,
                                 null=False)
    course_keywords = models.TextField(verbose_name="Keywords for this course",
                                       help_text="This is generated off of the file(s) input by the user.", blank=True,
                                       default="")
    created_on = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(default='',
                            editable=False)
    objects = CourseQuerySet.as_manager()

    def __str__(self):
        return "{0} - {1}".format(self.course_no, self.course_name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.course_name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('course_home', kwargs={'slug': self.slug, 'id': self.id})


class VideoInfo(models.Model):
    title = models.CharField(max_length=150,
                             blank=False,
                             null=False)
    added_by = models.ForeignKey(User,
                                 verbose_name="Name of the instructor",
                                 on_delete=models.CASCADE)
    video_description = models.TextField(max_length=1000, verbose_name="Description",
                                         help_text="Please add a description for this video.")
    video_url = models.CharField(max_length=500,
                                 verbose_name="URL for the video",
                                 help_text="This is generated automatically after the video file is "
                                           "uploaded by the user.",
                                 blank=False,
                                 null=False)
    hash_tags = models.CharField(max_length=150,
                                 verbose_name="Tags",
                                 help_text="Please add some tags separated by commas for better search"
                                           " capabilities. e.g. Programming, Design, Data Science, Neural Networks ",
                                 blank=False,
                                 null=False)
    video_keywords = models.TextField(verbose_name="Keywords for this video",
                                      help_text="This is generated off of the video and the index files"
                                                " input by the user.")
    video_thumbnail_url = models.CharField(max_length=500,
                                           verbose_name="URL for the video thumbnail",
                                           help_text="This is generated automatically after the video file"
                                                     " is uploaded by the user.",
                                           blank=False,
                                           null=False)
    video_length = models.CharField(max_length=12,
                                    verbose_name="Length of the video.",
                                    blank=False,
                                    null=False,
                                    help_text="This field is filled automatically after the video "
                                              "is uploaded by the user.")
    added_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    slug = models.SlugField(default='',
                            editable=False)
    objects = VideoQuerySet.as_manager()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return "{0}".format(self.title)


class FrequentlyAskedQuestion(VideoInfo):
    title = models.CharField(max_length=200,
                             blank=False,
                             null=False,
                             verbose_name="Question",
                             help_text="Please add a question."
                                       "(Should be less than 150 characters.)")
    views = models.ManyToManyField(User,
                                   related_name="view_count_faq",
                                   blank=True)
    likes = models.ManyToManyField(User,
                                   related_name="liked_users_faq",
                                   blank=True)

    def get_absolute_url(self):
        return reverse('faq_home', kwargs={'slug': self.slug, 'id': self.id})

    def get_api_like_url(self):
        return reverse('faq_like_toggle_api', kwargs={'id': self.id})


class Topic(VideoInfo):
    title = models.CharField(max_length=150,
                             blank=False,
                             null=False,
                             verbose_name="Topic Name",
                             help_text="Please add a topic name for this video. "
                                       "(Should be less than 150 characters.)")

    associated_courses = models.ManyToManyField(Course,
                                                verbose_name="Courses associated with this video.",
                                                help_text="Select all the courses that you want to "
                                                          "associate this topic with.")
    associated_faqs = models.ManyToManyField(FrequentlyAskedQuestion,
                                             verbose_name="FAQs associated with this video.",
                                             help_text="(Optional) Select all the FAQs that you want to "
                                                       "associate this topic with.",
                                             blank=True)
    difficulty_level = models.CharField(max_length=1,
                                        default="1",
                                        verbose_name="Difficulty level",
                                        help_text="Please add a difficulty level for this topic",
                                        choices=DIFFICULTY_LEVELS,
                                        blank=False,
                                        null=False)
    views = models.ManyToManyField(User,
                                   related_name="view_count_topic",
                                   blank=True)
    likes = models.ManyToManyField(User,
                                   related_name="liked_users_topic",
                                   blank=True)

    def get_absolute_url(self):
        return reverse('topic_home', kwargs={'slug': self.slug, 'id': self.id})

    def get_api_like_url(self):
        return reverse('topic_like_toggle_api', kwargs={'id': self.id})