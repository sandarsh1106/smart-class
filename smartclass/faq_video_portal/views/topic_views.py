from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import RedirectView
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.db.models import F
from PIL import Image

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User

import os
import pytesseract

from ..models import Course, Topic
from ..forms import TopicForm
from ..util import get_redirected
from smartclass.settings import MEDIA_ROOT
import machine_learning_utilities.multiword_keyword_extractor as mk
import machine_learning_utilities.video_upload_utility as vu


@login_required
def topic_home(request, slug, id):
    topic, topic_url = get_redirected(Topic, {'pk': id}, {'slug': slug})
    if topic_url:
        return redirect(topic_url)
    topic.views.add(request.user)
    related_topics = Topic.objects.related_videos(topic.hash_tags).exclude(pk=id)
    media_root = topic.video_url[:-9].replace('\\', '\\\\')
    keywords = [keyword.strip() for keyword in topic.video_keywords.split(',')]
    return render(request, 'faq_video_portal/home/topic_home.html', {'topic': topic,
                                                                     'related_topics': related_topics,
                                                                     'media_root': media_root,
                                                                     'keywords': keywords})


# Function to validate the topic.
@staff_member_required
def validate_topic(request):
    # Get the question
    topic = request.GET.get('title', None)

    # Prepare the data to be sent to ajax for processing.
    data = {

        # A bool variable to check whether the question already exists.
        'is_exists': topic.title() in Topic.objects.values_list('title', flat=True)
    }
    if data['is_exists']:
        data['error_message'] = 'This topic already exists! If this is a different video, ' \
                                'try reframing the topic name.'

    # Return the json response with the data to ajax for handling.
    return JsonResponse(data)


@staff_member_required
def add_topic(request):
    # post the data into the database only if the request type is POST.
    if request.method == "POST":

        # prepare the video form using TopicForm class.
        topic = Topic(added_by=request.user)
        form = TopicForm(instance=topic, data=request.POST)

        # continue only if the form data is valid.
        if form.is_valid():

            # check if the question is  present in the database already.
            if request.POST['title'].title() in Topic.objects.values_list('title', flat=True):

                # redirect the user to fill the form again since the data entered was already present in the database.
                messages.error(request, 'Topic already exists!')
                return redirect('add_topic')
            else:
                # Initialize keywords and index_phrases.
                keywords = ""
                index_phrases = set()

                # Get the associated courses for this topic and set it to the model.
                associated_courses = list(form.cleaned_data['associated_courses'])

                # Create a temporary directory to store all the topic related files
                # (transcripts, thumbnails, video, etc,.).
                temp_topic_path = MEDIA_ROOT + r'\temp_dir'
                os.mkdir(temp_topic_path)

                # Store all the index files in the same directory and fetch the index phrases.
                for count, file in enumerate(request.FILES.getlist("index_files")):
                    this_topic_path = temp_topic_path + r'\index_file_' + str(count) + '.png'
                    vu.file_save(this_topic_path, file)
                    tesseract_text = pytesseract.image_to_string(Image.open(this_topic_path))

                    # Make a call to ml utilities and extract the keywords.
                    index_phrases.update(mk.glossary_extract_to_string(tesseract_text).strip(" ").split(','))

                # Also use the index phrases stored in the associated courses.
                for course in associated_courses:
                    index_phrases.update(course.course_keywords.strip(" ").split(','))

                # Remove any empty strings.
                index_phrases = filter(None, index_phrases)

                # Fetch the video extension and save it as video.(extension).
                topic_name = 'topic' + request.FILES['video_file']._name[-4:]
                topic_path = "{0}\{1}".format(temp_topic_path, topic_name)
                vu.file_save(topic_path, request.FILES['video_file'])

                # Fetch the keywords from ML utility function and save it to model.
                keywords += mk.keywords_extractor_from_video(index_phrases,  os.path.join(temp_topic_path, topic_name))
                topic.video_keywords = keywords

                # Get the duration of the video and save it to the model.
                topic.video_length = vu.get_video_duration(os.path.join(temp_topic_path, topic_name))

                # Create a screenshot off of the video and store it in the same directory.
                vu.extract_image_from_video(os.path.join(temp_topic_path, topic_name),
                                            os.path.join(temp_topic_path, 'screenshot.png'))

                # Make thumbnail and save it.
                vu.make_thumbnail(temp_topic_path, 'screenshot.png')

                '''Set the video URL to temporary directory path for now. This is because, the video_url is a non-blank 
                and a non-nullable field. So, we cannot save the model without anything in it. Once the model is saved, 
                we shall rename the directory with the primary key appended to it, and re-save the model.
                This is same for thumbnails as well.'''
                topic.video_url = " "
                topic.video_thumbnail_url = " "

                # Save the form.
                form.save()

                # Fetch the video object with the current question.
                saved_topic = Topic.objects.get(title=form.cleaned_data['title'])

                # Prepare the final topic directory by adding the object's primary key to it.
                final_topic_dir_path = MEDIA_ROOT + r'\topic_dir_' + str(saved_topic.pk)

                # Rename the directory with the new path.
                os.rename(temp_topic_path, final_topic_dir_path)

                # Set the thumbnail and video URLs and save it.
                saved_topic.video_thumbnail_url = os.path.join(final_topic_dir_path, 'thumbnail.png')
                saved_topic.video_url = os.path.join(final_topic_dir_path, topic_name)
                saved_topic.save()
                return redirect('user_home')
    else:
        # Take the user to 'add-course' page if there are no courses in the database, so that he/she can add one.
        if Course.objects.count() == 0:
            messages.error(request, '"It seems like there are no courses yet. Please add one before proceeding!')
            return redirect('add_course')
        else:
            form = TopicForm()
            return render(request, 'faq_video_portal/forms/add_topic_form.html', {'form': form})


class TopicLikeToggleAPI(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, id):
        obj = get_object_or_404(Topic, id=id)
        user = request.user
        liked = False
        if user.is_authenticated:
            if user in obj.likes.all():
                obj.likes.remove(user)
                liked = False
            else:
                obj.likes.add(user)
                liked = True
        data = {"like_count": obj.likes.count(), "liked": liked}
        return Response(data)
