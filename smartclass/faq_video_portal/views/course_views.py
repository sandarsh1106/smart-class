from django.contrib.admin.views.decorators import staff_member_required
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib import messages
from PIL import Image
import os
import pytesseract
import shutil

from ..forms import CourseForm
from ..models import Course, FrequentlyAskedQuestion
from ..util import get_redirected
from smartclass.settings import MEDIA_ROOT
import machine_learning_utilities.multiword_keyword_extractor as mk
import machine_learning_utilities.video_upload_utility as vu

# Change this to your machine's directory. Without the directory specification, pytesseract won't work.
pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files (x86)\Tesseract-OCR\tesseract"


@login_required
def course_home(request, slug, id):
    course, course_url = get_redirected(Course, {'pk': id}, {'slug': slug})
    if course_url:
        return redirect(course_url)
    associated_topics = list(course.topic_set.all())
    related_courses = Course.objects.related_courses(course.hash_tags).exclude(id=id)
    associated_faqs = FrequentlyAskedQuestion.objects.faq_for_this_course(associated_topics)
    return render(request, 'faq_video_portal/home/course_home.html', {'course': course,
                                                                      'topics': associated_topics,
                                                                      'related_courses': related_courses,
                                                                      'faqs': associated_faqs})


# Function to validate the course number.
@staff_member_required
def validate_course_no(request):
    # Get the course number
    course_no = request.GET.get('course_no', None)

    # Prepare the data to be sent to ajax for processing.
    data = {

        # A bool variable to check whether the course number already exists.
        'is_exists': course_no.upper() in Course.objects.values_list('course_no', flat=True)
    }
    if data['is_exists']:
        data['error_message'] = 'Course number already exists!'

    # Return the json response with the data to ajax for handling.
    return JsonResponse(data)


# Function to validate the course name.
@staff_member_required
def validate_course_name(request):
    # Get the course number
    course_name = request.GET.get('course_name', None)

    # Prepare the data to be sent to ajax for processing.
    data = {

        # A bool variable to check whether the course name already exists.
        'is_exists': course_name.title() in Course.objects.values_list('course_name', flat=True)
    }
    if data['is_exists']:
        data['error_message'] = 'Course name already exists!'

    # Return the json response with the data to ajax for handling.
    return JsonResponse(data)


# view function to create a new course.
@staff_member_required
def add_course(request):
    # post the data into the database only if the request type is POST.
    if request.method == "POST":

        # prepare the course form using CourseForm class.
        course = Course(created_by=request.user)
        form = CourseForm(instance=course, data=request.POST)

        # continue only if the form data is valid.
        if form.is_valid():
            course_numbers = []
            course_names = []
            if Course.objects.count() > 0:

                # check if either of them are present in the database already.
                course_numbers, course_names = zip(*Course.objects.values_list('course_no', 'course_name'))
            if request.POST['course_no'].upper() in course_numbers \
                    or request.POST['course_name'].title() in course_names:

                # redirect the user to fill the form again since the data entered was already present in the database.
                messages.error(request, 'Course already exists!')
                return redirect('add_course')
            else:

                ''' Create the keywords list using glossary generator from the index file input. (If any.)
                And also save the index files for later uses in a directory with the course number. 
                Once the keywords are generated, stitch them into a long string and add it to course_keywords
                 field in the Course model.'''
                index_phrases = ""
                media_path = MEDIA_ROOT + r'\temp_dir'
                os.mkdir(media_path)
                for count, x in enumerate(request.FILES.getlist("index_files")):
                    this_file_path = media_path + r'\file_' + str(count) + '.png'
                    vu.file_save(this_file_path, x)
                    tesseract_text = pytesseract.image_to_string(Image.open(this_file_path))

                    # Make a call to ml utilities and extract the index phrases.
                    index_phrases += mk.glossary_extract_to_string(tesseract_text)

                # Remove the temporary directory completely. The files are not of any use to us.
                shutil.rmtree(media_path)

                # Save the index phrases to the model.
                course.course_keywords = index_phrases
                form.save()
                return redirect('user_home')
    else:
        form = CourseForm()
    return render(request, 'faq_video_portal/forms/add_course_form.html', {'form': form})
