from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import RedirectView
from django.shortcuts import get_object_or_404
from django.db.models import F

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User

import os
from PIL import Image
import pytesseract

from ..models import FrequentlyAskedQuestion
from ..forms import FAQForm
from ..util import get_redirected
from smartclass.settings import MEDIA_ROOT
import machine_learning_utilities.multiword_keyword_extractor as mk
import machine_learning_utilities.video_upload_utility as vu


@login_required
def faq_home(request, slug, id):
    faq, faq_url = get_redirected(FrequentlyAskedQuestion, {'pk': id}, {'slug': slug})
    if faq_url:
        return redirect(faq_url)
    faq.views.add(request.user)
    related_faqs = FrequentlyAskedQuestion.objects.related_videos(faq.hash_tags).exclude(pk=id)
    associated_topics = list(faq.topic_set.all())
    associated_courses = set()
    for topic in associated_topics:
        associated_courses.update(topic.associated_courses.all())
    media_root = faq.video_url[:-7].replace('\\', '\\\\')
    keywords = [keyword.strip() for keyword in faq.video_keywords.split(',')]
    return render(request, 'faq_video_portal/home/faq_home.html', {'faq': faq,
                                                                   'related_faqs': related_faqs,
                                                                   'associated_courses': associated_courses,
                                                                   'associated_topics': associated_topics,
                                                                   'media_root': media_root,
                                                                   'keywords': keywords})


# Function to validate the FAQ.
@staff_member_required
def validate_faq(request):
    # Get the question
    question = request.GET.get('title', None)

    # Prepare the data to be sent to ajax for processing.
    data = {

        # A bool variable to check whether the question already exists.
        'is_exists': question.title() in FrequentlyAskedQuestion.objects.values_list('title', flat=True)
    }
    if data['is_exists']:
        data['error_message'] = 'This question already exists! If this is a different video, ' \
                                'try reframing the question.'

    # Return the json response with the data to ajax for handling.
    return JsonResponse(data)


@staff_member_required
def add_faq(request):
    if request.method == "POST":

        # prepare the video form using FAQForm class.
        faq = FrequentlyAskedQuestion(added_by=request.user)
        form = FAQForm(instance=faq, data=request.POST)

        # continue only if the form data is valid.
        if form.is_valid():

            # check if the question is  present in the database already.
            if request.POST['title'].title() in FrequentlyAskedQuestion.objects.values_list('title', flat=True):

                # redirect the user to fill the form again since the data entered was already present in the database.
                messages.error(request, 'FAQ already exists!')
                return redirect('add_faq')
            else:
                # Initialize keywords and index_phrases.
                keywords = ""
                index_phrases = set()

                # Create a temporary directory to store all the faq related files
                # (transcripts, thumbnails, video, etc,.).
                temp_faq_path = MEDIA_ROOT + r'\temp_dir'
                os.mkdir(temp_faq_path)

                # Store all the index files in the same directory and fetch the index phrases.
                for count, file in enumerate(request.FILES.getlist("index_files")):
                    this_faq_path = temp_faq_path + r'\index_file_' + str(count) + '.png'
                    vu.file_save(this_faq_path, file)
                    tesseract_text = pytesseract.image_to_string(Image.open(this_faq_path))

                    # Make a call to ml utilities and extract the keywords.
                    index_phrases.update(mk.glossary_extract_to_string(tesseract_text).strip(" ").split(','))

                # Remove any empty strings.
                index_phrases = filter(None, index_phrases)

                # Fetch the video extension and save it as video.(extension).
                faq_name = 'faq' + request.FILES['video_file']._name[-4:]
                faq_path = "{0}\{1}".format(temp_faq_path, faq_name)
                vu.file_save(faq_path, request.FILES['video_file'])

                # Fetch the keywords from ML utility function and save it to model.
                keywords += mk.keywords_extractor_from_video(index_phrases,  os.path.join(temp_faq_path, faq_name))
                faq.video_keywords = keywords

                # Get the duration of the video and save it to the model.
                faq.video_length = vu.get_video_duration(os.path.join(temp_faq_path, faq_name))

                # Create a screenshot off of the video and store it in the same directory.
                vu.extract_image_from_video(os.path.join(temp_faq_path, faq_name),
                                            os.path.join(temp_faq_path, 'screenshot.png'))

                # Make thumbnail and save it.
                vu.make_thumbnail(temp_faq_path, 'screenshot.png')

                '''Set the video URL to temporary directory path for now. This is because, the video_url is a non-blank 
                and a non-nullable field. So, we cannot save the model without anything in it. Once the model is saved, 
                we shall rename the directory with the primary key appended to it, and re-save the model.
                This is same for thumbnails as well.'''
                faq.video_url = " "
                faq.video_thumbnail_url = " "

                # Save the form.
                form.save()

                # Fetch the video object with the current question.
                saved_faq = FrequentlyAskedQuestion.objects.get(title=form.cleaned_data['title'])

                # Prepare the final faq directory by adding the object's primary key to it.
                final_faq_dir_path = MEDIA_ROOT + r'\faq_dir_' + str(saved_faq.pk)

                # Rename the directory with the new path.
                os.rename(temp_faq_path, final_faq_dir_path)

                # Set the thumbnail and video URLs and save it.
                saved_faq.video_thumbnail_url = os.path.join(final_faq_dir_path, 'thumbnail.png')
                saved_faq.video_url = os.path.join(final_faq_dir_path, faq_name)
                saved_faq.save()
                return redirect('user_home')
    else:
        form = FAQForm()
        return render(request, 'faq_video_portal/forms/add_faq_form.html', {'form': form})


class FAQLikeToggleAPI(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, id):
        obj = get_object_or_404(FrequentlyAskedQuestion, id=id)
        user = request.user
        liked = False
        if user.is_authenticated:
            if user in obj.likes.all():
                obj.likes.remove(user)
                liked = False
            else:
                obj.likes.add(user)
                liked = True
        data = {"like_count": obj.likes.count(), "liked": liked}
        return Response(data)

