from django.apps import AppConfig


class FaqVideoPortalConfig(AppConfig):
    name = 'faq_video_portal'
