# Generated by Django 2.0.4 on 2018-05-17 05:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq_video_portal', '0003_auto_20180517_0032'),
    ]

    operations = [
        migrations.RenameField(
            model_name='video',
            old_name='video_url',
            new_name='video_file',
        ),
        migrations.RenameField(
            model_name='video',
            old_name='video_thumbnail_url',
            new_name='video_thumbnail_file',
        ),
    ]
