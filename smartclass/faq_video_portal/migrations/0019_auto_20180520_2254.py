# Generated by Django 2.0.4 on 2018-05-21 03:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('faq_video_portal', '0018_auto_20180520_2201'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='video_length',
            field=models.CharField(help_text='This field is filled automatically after the video is uploaded by the user.', max_length=12, verbose_name='Length of the video.'),
        ),
    ]
