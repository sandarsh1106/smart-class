# Generated by Django 2.0.4 on 2018-05-28 13:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('faq_video_portal', '0030_auto_20180527_2215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='frequentlyaskedquestion',
            name='slug',
            field=models.SlugField(default='', editable=False),
        ),
        migrations.AlterField(
            model_name='topic',
            name='slug',
            field=models.SlugField(default='', editable=False),
        ),
    ]
