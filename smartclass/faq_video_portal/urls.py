from django.urls import re_path

from .views.course_views import *
from .views.topic_views import *
from .views.faq_views import *

urlpatterns = [
    re_path(r'^add-course/$', add_course, name='add_course'),
    re_path(r'^add-topic/$', add_topic, name='add_topic'),
    re_path(r'^add-faq/$', add_faq, name='add_faq'),
    re_path(r'^courses/(?P<id>\d+)/(?P<slug>[-\w\d]+)/$', course_home, name='course_home'),
    re_path(r'^topics/(?P<id>\d+)/(?P<slug>[-\w\d]+)/$', topic_home, name='topic_home'),
    re_path(r'^faqs/(?P<id>\d+)/(?P<slug>[-\w\d]+)/$', faq_home, name='faq_home'),
    re_path(r'^ajax/validate-course-no/$', validate_course_no, name='validate_course_no'),
    re_path(r'^ajax/validate-course-name/$', validate_course_name, name='validate_course_name'),
    re_path(r'^ajax/validate-topic/$', validate_topic, name='validate_topic'),
    re_path(r'^ajax/validate-faq/$', validate_faq, name='validate_faq'),
    re_path(r'^api/faqs/like/(?P<id>\d+)/$', FAQLikeToggleAPI.as_view(), name='faq_like_toggle_api'),
    re_path(r'^api/topics/like/(?P<id>\d+)/$', TopicLikeToggleAPI.as_view(), name='topic_like_toggle_api')
]
