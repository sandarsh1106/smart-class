from django.forms import ModelForm

from .models import Course, Topic, FrequentlyAskedQuestion


class CourseForm(ModelForm):
    class Meta:
        model = Course
        exclude = ('course_keywords', 'created_by', 'created_on', 'slug')

    def clean_course_no(self):
        return self.cleaned_data['course_no'].upper()

    def clean_course_name(self):
        return self.cleaned_data['course_name'].title()


class TopicForm(ModelForm):
    class Meta:
        model = Topic
        fields = ['title', 'video_description', 'difficulty_level', 'hash_tags', 'associated_courses',
                  'associated_faqs']
        exclude = ('video_keywords', 'added_by', 'added_on', 'video_url', 'video_thumbnail_url', 'video_length',
                   'modified_on', 'views', 'likes', 'slug')

    def clean_title(self):
        return self.cleaned_data['title'].title()

    def clean_video_description(self):
        return self.cleaned_data['video_description'].capitalize()


class FAQForm(ModelForm):
    class Meta:
        model = FrequentlyAskedQuestion
        fields = ['title', 'video_description', 'hash_tags']
        exclude = ('video_keywords', 'added_by', 'added_on', 'video_url', 'video_thumbnail_url', 'video_length',
                   'modified_on', 'views', 'likes', 'slug')

        def clean_title(self):
            return self.cleaned_data['title'].title()

        def clean_video_description(self):
            return self.cleaned_data['video_description'].capitalize()

