from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.auth import login, authenticate

from .forms import SignUpForm
from faq_video_portal import models as fp


@login_required
def home(request):
    if request.user.is_staff:

        return render(request, 'users/home.html')
    else:
        return render(request, 'users/home.html')


@login_required
def validate_username(request):
    username = request.GET.get('username', None)
    data = {
        'is_exists': User.objects.filter(username__iexact=username).exists()
    }
    if data['is_exists']:
        data['error_message'] = 'This username is already taken!'
    return JsonResponse(data)


@login_required
def change_password(request):
    if request.method == "POST":
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your Password was successfully updated!')
            return redirect('user_home')
        else:
            messages.error(request, 'Please correct the error below!')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'users/change_password.html', {'form': form})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('user_home')
    else:
        form = SignUpForm()
    return render(request, 'users/signup_form.html', {'form': form})


@login_required
def author_home(request, id):
    author = get_object_or_404(User, pk=id)
    if not author.is_staff:
        return redirect('user_home')
    courses = list(author.course_set.all())
    topics = list(author.topic_set.all())
    faqs = list(author.frequentlyaskedquestion_set.all())
    return render(request, 'faq_video_portal/home/author_home.html', {'author': author,
                                                                      'courses': courses,
                                                                      'topics': topics,
                                                                      'faqs': faqs})
