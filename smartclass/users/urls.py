from django.urls import re_path
from django.contrib.auth.views import LoginView, LogoutView

from .views import signup, home, validate_username, change_password, author_home


urlpatterns = [
    re_path(r'^home/$', home, name="user_home"),
    re_path(r'^login/$', LoginView.as_view(template_name='users/login_form.html'), name='user_login'),
    re_path(r'^logout/$', LogoutView.as_view(), name='user_logout'),
    re_path(r'^signup/$', signup, name='user_signup'),
    re_path(r'^change-password/$', change_password, name='user_change_password'),
    re_path(r'^author-home/(?P<id>\d+)$', author_home, name='author_home'),
    re_path(r'^ajax/validate-username/$', validate_username, name='validate_username'),
]