# Smart Class

Before running the project, Follow these steps to avoid running into problems.

Step.1: Create a new text file and rename it to db.sqlite3 in smartclass/

Step.2: Run the following command to install the required packages: 
	
		pip install --upgrade -r Requirements.txt

Step.3: Open Python console, and type the following:

		import nltk
		nltk.download()

Note: This will open a dialog box to download the required nltk packages.
Download all packages under collection tab so that you don't run into
any problems in the future.

Step.4: Once you have successfully installed all the packages, copy 'smartstoplist' file from the root folder to the following file path:

		'C:/User/{username}/AppData/Roaming/nltk_data/corpora/stopwords/'

Note: This file path only works on Windows and it is assumed that nltk package was installed in default directory. If the file path was different, make sure to change it accordingly.

Step.5: Since you just created the database, the schema isn't ready. So, you need to run the migrations to setup the initial schema. You can do that by running 'python manage.py migrate' in your terminal.

Note: If you get an error from Pytesseract module, Go to smartclass/faqvideoportal/course_views.py and change the following: 
		
		pytesseract.pytesseract.tesseract_cmd = '{your pytesseract installation folder/tesseract/}'

Ignore this, if you don't get any errors from Pytesseract.

Happy coding!